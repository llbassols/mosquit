package cat.dam.lluc.mosquitattac;

import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.graphics.drawable.AnimationDrawable;
import android.widget.TextView;
import android.os.Handler;

public class MainActivity extends AppCompatActivity {
    ImageView iv_mosquit;
    AnimationDrawable mosquit_animat;
    ConstraintLayout main_screen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mosquit_animat = new AnimationDrawable();
        iv_mosquit = (ImageView) findViewById(R.id.iv_mosquit);
        final TextView textView1 = (TextView)findViewById(R.id.textView);
        // Situem la imatge en la pantalla
        final CountDownTimer timer = new CountDownTimer(20000, 1000) {
            public void onTick(long millisUntilFinished) {
                textView1.setText("FALTEN: " + millisUntilFinished / 1000);
                int numerox = (int) (Math.random() * 1000) + 1;
                int numeroy = (int) (Math.random() * 2000) + 1;

                iv_mosquit.setX(numerox);
                iv_mosquit.setY(numeroy);
            }

            public void onFinish() {
                textView1.setText("HAS ACABAT");
            }

        }.start();
        iv_mosquit.setBackgroundResource(R.drawable.mosquit_animat);
        // Obté el fons que ha estat compilat amb un objecte AnimationDrawable
        mosquit_animat = (AnimationDrawable) iv_mosquit.getBackground();
        // Comença l'animació (per defecte repetició de cicle).
        mosquit_animat.start();
        iv_mosquit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // En cas de que es cliqui el mosquit actualiza el fons d'imatge amb el recurs XML on es defineix les imatges
                // i temps d'animació de la taca de sang
                iv_mosquit.setBackgroundResource(R.drawable.sang_animat);
                mosquit_animat = (AnimationDrawable) iv_mosquit.getBackground();
                // Fes l'animació (només un cicle).
                mosquit_animat.start();
                timer.cancel();

                /*timer.start();
                iv_mosquit.setBackgroundResource(R.drawable.mosquit_animat);
                mosquit_animat = (AnimationDrawable) iv_mosquit.getBackground();
                mosquit_animat.start();*/

            }
        });
    }
}
